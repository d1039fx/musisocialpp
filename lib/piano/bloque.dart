import 'dart:async';

import 'package:flutter/material.dart';
import 'package:musisocial/clases/teclas.dart';
import 'package:musisocial/provider/post_acciones.dart';
import 'package:musisocial/provider/soundpool_manager.dart';

class Instrumento extends StatefulWidget {
  final PostAcciones postAcciones;

  const Instrumento({Key key, this.postAcciones}) : super(key: key);

  @override
  _InstrumentoState createState() => _InstrumentoState();
}

class _InstrumentoState extends State<Instrumento> {
  SoundpoolManager _soundpoolManager = SoundpoolManager();
  Stopwatch _stopwatch = Stopwatch();
  Teclas _teclas = Teclas();
  List<TableRow> _listaGrupo;
  List<TableRow> _listaGrupoRestante;

  TableRow _grupoTeclas(
      {PostAcciones postAcciones,
      SoundpoolManager soundpoolManager,
      int inicia,
      int finaliza}) {
    return TableRow(
        children: soundpoolManager.listaLoadGet
            .sublist(inicia, finaliza)
            .map((e) => FutureBuilder<int>(
                future: e,
                builder: (context, datos) {
                  return Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: RaisedButton(
                        padding: const EdgeInsets.all(0),
                        color: Color.fromRGBO(0, 0,
                            4 * soundpoolManager.listaLoadGet.indexOf(e), 1),
                        child: Text(
                          (soundpoolManager.listaLoadGet.indexOf(e) + 1)
                              .toString(),
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                        onPressed: () {
                          soundpoolManager
                              .playSoundList(idSonido: datos.data)
                              .whenComplete(() {
                            postAcciones.teclaTocadaSet =
                                (soundpoolManager.listaLoadGet.indexOf(e) + 1)
                                    .toString();
                            postAcciones.listaTeclasTocadasSet =
                                soundpoolManager.listaLoadGet.indexOf(e) + 1;
                            if (_stopwatch.isRunning) {
                              postAcciones.listaTiempoSet = {
                                'tecla':
                                    soundpoolManager.listaLoadGet.indexOf(e) +
                                        1,
                                'tiempo': _stopwatch.elapsedMilliseconds
                              };
                            }
                          });
                        }),
                  );
                }))
            .toList());
  }

  void _reset() {
    setState(() {
      if (_stopwatch.isRunning) {
        print('${_stopwatch.elapsedMilliseconds}');
      } else {
        _stopwatch.reset();
      }
    });
  }

  void _stopStart() {
    setState(() {
      if (_stopwatch.isRunning) {
        _stopwatch.stop();
      } else {
        _stopwatch.start();
      }
    });
  }

  iniciarEjecucion({int tiempo}) {
    for (int i = 0; i < tiempo; i++) {
      widget.postAcciones.listaTiempoGet.forEach((elemento) {
        if (elemento['tiempo'] == i) {
          Future.delayed(Duration(milliseconds: i), () {
            print('se toco: $i');
            _soundpoolManager
                .playSoundList(idSonido: elemento['tecla'])
                .whenComplete(() {
              widget.postAcciones.teclaTocadaSet = elemento['tecla'].toString();
            });
          });
        }
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _soundpoolManager.listasonidosSet = _teclas.listaTeclasGrupo1();
    _listaGrupo = [];
    _listaGrupoRestante = [];

    int teclas = _teclas.listaTeclasGrupo1().length;
    for (int i = 0; i < teclas; i++) {
      if ((i % 10) == 0) {
        if (i <= 50) {
          print('inicio: $i, fin: ${i + 10}');
          _listaGrupo.add(_grupoTeclas(
              postAcciones: widget.postAcciones,
              soundpoolManager: _soundpoolManager,
              inicia: i,
              finaliza: i + 10));
        } else {
          print('fin: $i, total: $teclas');
          _listaGrupoRestante.add(_grupoTeclas(
              postAcciones: widget.postAcciones,
              soundpoolManager: _soundpoolManager,
              inicia: i,
              finaliza: ((teclas - 1) - i) + i));
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    print(this);
    return Scaffold(
      appBar: AppBar(
        title: Text('Instrumento'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Tecla que se toco: ${widget.postAcciones.teclaTocadaGet}',
            ),
            Text(widget.postAcciones.listaTeclasTocadasGet.toString()),
            TiempoEjecucion(),
            Text(_stopwatch.elapsed.inMilliseconds.toString()),
            Table(
              children: _listaGrupo,
            ),
            Table(
              children: _listaGrupoRestante,
            )
          ],
        ),
      ),
      bottomSheet: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          ElevatedButton(
            onPressed: _stopStart,
            child: Text('start/stop grabacion'),
          ),
          _stopwatch.isRunning
              ? Container(
                  child: Text('Grabando'),
                )
              : ElevatedButton(
                  child: Text('Ejecutar grabacion'),
                  onPressed: () =>
                      iniciarEjecucion(tiempo: _stopwatch.elapsedMilliseconds)),
          ElevatedButton(
              child: Text('Remover ultimo'),
              onPressed: () {
                widget.postAcciones.listaTeclasTocadasSet = 0;
              }),
          ElevatedButton(
              child: Text('limpiar lista y reiniciar'),
              onPressed: () {
                widget.postAcciones.listaTeclasTocadasSet = null;
                widget.postAcciones.listaTiempoGet.clear();
                _reset();
              }),
        ],
      ),
    );
  }
}

class TiempoEjecucion extends StatefulWidget {
  @override
  _TiempoEjecucionState createState() => _TiempoEjecucionState();
}

class _TiempoEjecucionState extends State<TiempoEjecucion> {
  String _timeString;

  void _getCurrentTime() {
    setState(() {
      _timeString =
          "${DateTime.now().hour} : ${DateTime.now().minute} :${DateTime.now().second}";
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _timeString =
        '${DateTime.now().hour} : ${DateTime.now().minute} :${DateTime.now().second}';
    Timer.periodic(Duration(seconds: 1), (timer) => _getCurrentTime());
  }

  @override
  Widget build(BuildContext context) {
    return Text(_timeString);
  }
}
