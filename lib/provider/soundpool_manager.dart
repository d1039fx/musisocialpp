import 'package:flutter/services.dart';
import 'package:soundpool/soundpool.dart';

class SoundpoolManager {
  Soundpool _soundpool = Soundpool.fromOptions();

  List<Future<int>> _listaLoad = [];

  List<Future<int>> get listaLoadGet => _listaLoad;

  set listaLoadSet(Future<int> value) {
    _listaLoad.add(value);
  }

  //lista de sonidos
  List<Map<String, String>> _listasonidos = [];

  List<Map<String, String>> get listasonidosGet => _listasonidos;

  set listasonidosSet(List<Map<String, String>> value) {
    value.forEach((element) {
      listaLoadSet = loadSound(assetFile: element['tecla']);
    });
  }

  double _rate = 1.0;

  //assets
  Future<int> _soundDataLoad;

  ///muestra los datos del sonido
  Future<int> get soundDataLoadGet => _soundDataLoad;

  ///funcion para leer el sonido a partir del assets
  Future<int> loadSound({String assetFile}) async {
    ByteData asset = await rootBundle.load(assetFile);
    return await _soundpool.load(asset);
  }

  ///funcion para ejecutar listas de sonido
  Future<void> playSoundList({int idSonido}) async {
    return await _soundpool.play(idSonido);
  }

  //url/////////////////////////////////////////////////////////////////////////////////////////////
  Future<int> _soundUrl;

  ///muestra el sonido a partir de una url
  Future<int> get soundUrlGet => _soundUrl;

  ///trae el sonido a partir de una url
  set soundUrlSet(String value) {
    _soundUrl = loadCheering(urlSonido: value);
  }

  int _cheeringStreamId = -1;

  int get cheeringStreamIdGet => _cheeringStreamId;

  set cheeringStreamIdSet(int value) {
    _cheeringStreamId = value;
  }

  Future<int> _cheeringId;

  Future<int> get cheeringIdGet => _cheeringId;

  set cheeringIdSet(Future<int> value) {
    _cheeringId = value;
  }

  ///funcion para leer el sonido a partir del url(funciona solo en android)
  Future<int> loadCheering({String urlSonido}) async {
    cheeringIdSet = _soundpool.loadUri(urlSonido);
    return await _soundpool.loadUri(urlSonido);
  }

  Future<void> playCheering() async {
    int _sound = await cheeringIdGet;
    cheeringStreamIdSet = await _soundpool.play(_sound, rate: _rate);
  }

  Future<void> updateCheeringRate() async {
    if (cheeringStreamIdGet > 0) {
      await _soundpool.setRate(
          streamId: cheeringStreamIdGet, playbackRate: _rate);
    }
  }

  ///cambia el volumen
  Future<void> updateVolume({double volumenNivel}) async {
    int _cheeringSound = await cheeringIdGet;
    _soundpool.setVolume(soundId: _cheeringSound, volume: volumenNivel);
  }
}
