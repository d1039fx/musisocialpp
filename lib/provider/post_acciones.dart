import 'package:flutter/material.dart';

class PostAcciones with ChangeNotifier {
  String _teclaTocada = '';

  String get teclaTocadaGet => _teclaTocada;

  set teclaTocadaSet(String value) {
    _teclaTocada = value;
    notifyListeners();
  }

  List<int> _listaTeclasTocadas = [];

  List<int> get listaTeclasTocadasGet => _listaTeclasTocadas;

  set listaTeclasTocadasSet(int value) {
    if (value == 0) {
      _listaTeclasTocadas.removeLast();
    } else if (value == null) {
      _listaTeclasTocadas.clear();
    } else {
      _listaTeclasTocadas.add(value);
    }
    notifyListeners();
  }

  List<Map<String, int>> _listaTiempo = [];

  List<Map<String, int>> get listaTiempoGet => _listaTiempo;

  set listaTiempoSet(Map<String, int> value) {
    _listaTiempo.add(value);
    //notifyListeners();
  }



}
