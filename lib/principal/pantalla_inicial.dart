import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:musisocial/piano/bloque.dart';
import 'package:musisocial/provider/post_acciones.dart';
import 'package:provider/provider.dart';
import 'package:soundpool/soundpool.dart';

class MyApp extends StatelessWidget {
  final Soundpool soundpool;

  const MyApp({Key key, this.soundpool}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [

        ChangeNotifierProvider<PostAcciones>(create: (_) => PostAcciones())
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Ints(),
      ),
    );
  }
}

class Ints extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    PostAcciones _postAcciones = Provider.of<PostAcciones>(context);
    return Instrumento(postAcciones: _postAcciones,);
  }
}