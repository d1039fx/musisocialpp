# musisocialpp

Esta App se hace con la intencion de poder crear instrumentos  
musicales no solo usando sonidos de intrumentos musicales  
sino tambien en lo posible de cualquier objeto.

## Modelo

- **Instrumento**
    - Nombre (tipo: String)
    - Descripcion (tipo: String)
    - **Teclas (tipo: List)**
        - Nombre (tipo: String)
        - Sonido (tipo: Object)

## Funcionalidad General

1. Crear Intrumento
2. Crear Sonido
    1. **Instrumento Creado**
        1. Crear Tonada con el instrumento
        2. **Instrumentos creados**
            1. Combinar tonadas creadas por los intrumentos
